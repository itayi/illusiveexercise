#include "Utilities.h"

string Utilities::formatBytes(DWORDLONG bytes)
{
	const short BYTES_PER_KILO = 1024;
	const string* BYTES_MULTIPLIERS = new string[5]{ "B", "KB", "MB", "GB", "TB" };

	int bytesMultipliersIndex = 0;
	double bytesDouble = bytes;
	while (bytesDouble / BYTES_PER_KILO >= 1)
	{
		bytesDouble /= BYTES_PER_KILO;
		bytesMultipliersIndex++;
	}

	stringstream bytesDoubleString;
	bytesDoubleString << std::fixed << std::setprecision(2) << bytesDouble;

	return bytesDoubleString.str() + BYTES_MULTIPLIERS[bytesMultipliersIndex];
}