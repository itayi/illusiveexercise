#ifndef _UTILITIES_
#define _UTILITIES_

#include <string>
#include <iomanip>
#include <sstream>
#include <Windows.h>

using namespace std;

class Utilities
{

private:
	
	const string* BYTES_MULTIPLIERS = new string[5]{ "B", "KB", "MB", "GB", "TB" };

	Utilities(const Utilities& other) = default; // Prevent the copy constructor from being used
	const Utilities& operator=(const Utilities& other) {}; // Prevent the = operator from being used

public:

	static string formatBytes(DWORDLONG bytes);
};

#endif
