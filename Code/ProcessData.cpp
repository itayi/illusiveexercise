#include "ProcessData.h"

ProcessData::ProcessData(DWORD pid, HANDLE processHandle)
{
	this->processID = pid;

	TCHAR processPath[MAX_PATH];
	if (GetModuleFileNameEx(processHandle, 0, processPath, MAX_PATH))
	{
		this->processFullPath = processPath;
		this->isValid = true;
	}
	else
	{
		this->isValid = false;
	}

	PROCESS_MEMORY_COUNTERS_EX pmc;
	GetProcessMemoryInfo(processHandle, (PPROCESS_MEMORY_COUNTERS)&pmc, sizeof(pmc));
	this->virtualMemoryInUse = pmc.PrivateUsage;
	this->physicalMemoryInUse = pmc.WorkingSetSize;
}

void ProcessData::writeProcessDataToFile(ofstream &file)
{
	file << "Process ID: " << this->processID << endl;
	file << "Full Path: " << this->processFullPath << endl;
	file << "Virtual Memory In Use: " << Utilities::formatBytes(this->virtualMemoryInUse) << endl;
	file << "Physical Memory In Use: " << Utilities::formatBytes(this->physicalMemoryInUse) << endl;
}

DWORD ProcessData::getProcessID()
{
	return this->processID;
}


string ProcessData::getProcessFullPath()
{
	return this->processFullPath;
}

SIZE_T ProcessData::getVirtualMemoryInUse()
{
	return this->virtualMemoryInUse;
}

SIZE_T ProcessData::getPhysicalMemoryInUse()
{
	return this->physicalMemoryInUse;
}

bool ProcessData::isValidProcess()
{
	return this->isValid;
}