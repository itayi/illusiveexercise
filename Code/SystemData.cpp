#include "SystemData.h"

SystemData::SystemData()
{
	MEMORYSTATUSEX memInfo;
	memInfo.dwLength = sizeof(MEMORYSTATUSEX);
	GlobalMemoryStatusEx(&memInfo);

	this->totalVirtualMemory = memInfo.ullTotalPageFile;
	this->virtualMemoryInUse = memInfo.ullTotalPageFile - memInfo.ullAvailPageFile;
	this->totalPhysicalMemory = memInfo.ullTotalPhys;
	this->physicalMemoryInUse = memInfo.ullTotalPhys - memInfo.ullAvailPhys;
	this->cpuPercentageUsage = this->getCPULoad() * 100;
}

void SystemData::writeSystemDataToFile(ofstream &file)
{
	file << "Total Virtual Memory: " << Utilities::formatBytes(this->totalVirtualMemory) << endl;
	file << "Virtual Memory In Use: " << Utilities::formatBytes(this->virtualMemoryInUse) << endl;
	file << "Total Physical Memory: " << Utilities::formatBytes(this->totalPhysicalMemory) << endl;
	file << "Physical Memory In Use: " << Utilities::formatBytes(this->physicalMemoryInUse) << endl;
	file << "CPU Percentage Usage: " << this->cpuPercentageUsage << "%" << endl;
}

float SystemData::calculateCPULoad(unsigned long long idleTicks, unsigned long long totalTicks)
{
	static unsigned long long previousTotalTicks = 0;
	static unsigned long long previousIdleTicks = 0;

	unsigned long long totalTicksSinceLastTime = totalTicks - previousTotalTicks;
	unsigned long long idleTicksSinceLastTime = idleTicks - previousIdleTicks;

	float ret = 1.0f - ((totalTicksSinceLastTime > 0) ? ((float)idleTicksSinceLastTime) / totalTicksSinceLastTime : 0);

	previousTotalTicks = totalTicks;
	previousIdleTicks = idleTicks;
	return ret;
}

unsigned long long SystemData::fileTimeToInt64(const FILETIME & ft) 
{ 
	return (((unsigned long long)(ft.dwHighDateTime)) << 32) | ((unsigned long long)ft.dwLowDateTime); 
}

float SystemData::getCPULoad()
{
	FILETIME idleTime, kernelTime, userTime;
	return GetSystemTimes(&idleTime, &kernelTime, &userTime) ? this->calculateCPULoad(this->fileTimeToInt64(idleTime), this->fileTimeToInt64(kernelTime) + this->fileTimeToInt64(userTime)) : -1.0f;
}