#include "FindSuspiciousProcesses.h"

void main(int argumentCount, char *argumentValues[])
{
	FindSuspiciousProcesses* mainProgram = new FindSuspiciousProcesses(argumentValues[1]);

	mainProgram->run();
}
