#ifndef _SYSTEM_DATA_
#define _SYSTEM_DATA_

#include <windows.h>
#include <fstream>
#include "Utilities.h"
#include <iostream>

using namespace std;

class SystemData
{

	private:

		DWORDLONG totalVirtualMemory;
		DWORDLONG totalPhysicalMemory;
		DWORDLONG virtualMemoryInUse;
		DWORDLONG physicalMemoryInUse;
		double cpuPercentageUsage;

		SystemData(const SystemData& other) = default; // Prevent the copy constructor from being used
		const SystemData& operator=(const SystemData& other) {}; // Prevent the = operator from being used

		float calculateCPULoad(unsigned long long idleTicks, unsigned long long totalTicks);
		unsigned long long fileTimeToInt64(const FILETIME & ft);
		float getCPULoad();

	public:

		SystemData();
		void writeSystemDataToFile(ofstream &file);
		DWORDLONG getTotalVirtualMemory();
		DWORDLONG getTotalPhysicalMemory();
		DWORDLONG getVirtualMemoryInUse();
		DWORDLONG getPhysicalMemoryInUse();
		double getCPUPercentageUsage();

};

#endif