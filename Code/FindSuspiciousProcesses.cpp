#include "FindSuspiciousProcesses.h"

FindSuspiciousProcesses::FindSuspiciousProcesses(char* ipAddressToCheck)
{
	this->ipAddressToCheck = ipAddressToCheck;
	this->errorMessage = "";
}

string FindSuspiciousProcesses::getTodaysDateTime()
{
	char buffer[70];
	time_t rawtime;
	struct tm timeinfo;
	time(&rawtime);
	localtime_s(&timeinfo, &rawtime);
	strftime(buffer, 100, "%d-%m-%Y-%H-%M-%S", &timeinfo);
	string str(buffer);

	return str;
}

void FindSuspiciousProcesses::writeDataToFile()
{
	string fileName = "suspiciousProcess-" + getTodaysDateTime() + ".txt";
	ofstream outputFile(fileName);

	if (!this->errorMessage.empty())
	{
		outputFile << errorMessage;
	}
	else
	{
		const string breaker = "#############";
		outputFile << breaker << endl << "System Data:" << endl << breaker << endl;
		this->currentSystemData->writeSystemDataToFile(outputFile);
		outputFile << endl << breaker << endl << "Proceses Data:" << endl << breaker << endl;

		for (auto const &process : this->processDataList)
		{
			if (process->isValidProcess())
			{
				process->writeProcessDataToFile(outputFile);
			}
		}
	}

	outputFile.close();
}

void FindSuspiciousProcesses::getAllProcessIDsConnectedToIP()
{
	DWORD ipAddressToCheckAsDWORD = inet_addr(this->ipAddressToCheck);
	DWORD tcpTableSize;
	DWORD getTCPTableResult;
	MIB_TCPTABLE_OWNER_PID* tcpTableData;
	MIB_TCPROW_OWNER_PID* tcpTableRow;
	
	// Get the size of the table first, then allocate memory accordingly and get the table itself
	getTCPTableResult = GetExtendedTcpTable(NULL, &tcpTableSize, false, AF_INET, TCP_TABLE_OWNER_PID_ALL, 0);
	tcpTableData = (MIB_TCPTABLE_OWNER_PID*)malloc(tcpTableSize);
	getTCPTableResult = GetExtendedTcpTable(tcpTableData, &tcpTableSize, false, AF_INET, TCP_TABLE_OWNER_PID_ALL, 0);

	if (getTCPTableResult == NO_ERROR)
	{
		for (DWORD i = 0; i < tcpTableData->dwNumEntries; i++) 
		{
			tcpTableRow = &tcpTableData->table[i];
			
			if (tcpTableRow->dwRemoteAddr == ipAddressToCheckAsDWORD)
			{
				this->processIDSet.insert(tcpTableRow->dwOwningPid);
			}
		}
	}
	else
	{
		this->errorMessage = "ERROR GETTING TCP CONNECTIONS!";
	}

	free(tcpTableData);
}


void FindSuspiciousProcesses::createProcessDataList()
{
	for (DWORD const &pid : this->processIDSet)
	{
		HANDLE currentProcessHandle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);
		if (currentProcessHandle)
		{
			this->processDataList.push_back(new ProcessData(pid, currentProcessHandle));
			CloseHandle(currentProcessHandle);
		}
	}
}

void FindSuspiciousProcesses::run()
{
	this->currentSystemData = new SystemData();
	set<DWORD> processIDList;
	getAllProcessIDsConnectedToIP();
	createProcessDataList();
	writeDataToFile();
}