#ifndef _FIND_SUSPICIOUS_PROCESS_
#define _FIND_SUSPICIOUS_PROCESS_

#include <windows.h>
#include <iphlpapi.h>
#include <stdio.h>
#include <stdlib.h>
#include <tchar.h>
#include <psapi.h>
#include <iostream> 
#include <fstream>
#include <ctime>
#include <tchar.h>
#include <Pdh.h>
#include <set>
#include <vector>
#include <iomanip>
#include <string>
#include "ProcessData.h"
#include "SystemData.h"
#pragma comment(lib, "Iphlpapi")
#pragma comment(lib, "Pdh")
#pragma comment(lib, "ws2_32.lib")

using namespace std;

class FindSuspiciousProcesses
{

	private:

		char* ipAddressToCheck;
		SystemData* currentSystemData;
		set<DWORD> processIDSet;
		vector<ProcessData*> processDataList;
		string errorMessage;

		FindSuspiciousProcesses(const FindSuspiciousProcesses& other) = default; // Prevent the copy constructor from being used
		const FindSuspiciousProcesses& operator=(const FindSuspiciousProcesses& other) {}; // Prevent the = operator from being used

		string getTodaysDateTime();
		void writeDataToFile();
		void getAllProcessIDsConnectedToIP();
		void createProcessDataList();
		void writeSystemAndProcessDataToFile();

	public:

		FindSuspiciousProcesses(char* ipAddressToCheck);
		static string formatBytes(unsigned long bytes);
		void run();
};

#endif
