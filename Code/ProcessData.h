#ifndef _PROCESS_DATA_
#define _PROCESS_DATA_

#include <windows.h>
#include <string>
#include <psapi.h>
#include <Wtsapi32.h>
#include <vector>
#include <iostream>
#include <fstream>
#include "Utilities.h"
#pragma comment(lib, "Wtsapi32.lib")

using namespace std;

class ProcessData
{

	private:

		bool isValid;
		DWORD processID;
		string processFullPath;
		SIZE_T virtualMemoryInUse;
		SIZE_T physicalMemoryInUse;

		ProcessData(const ProcessData& other) = default; // Prevent the copy constructor from being used
		const ProcessData& operator=(const ProcessData& other) {}; // Prevent the = operator from being used

	public:

		ProcessData(DWORD pid, HANDLE processHandle);
		bool isValidProcess();
		void writeProcessDataToFile(ofstream &file);
		DWORD getProcessID();
		string getProcessFullPath();
		SIZE_T getVirtualMemoryInUse();
		SIZE_T getPhysicalMemoryInUse();
};

#endif