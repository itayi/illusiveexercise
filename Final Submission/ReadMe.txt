~~~~~~~~~~~~~
Instructions:
~~~~~~~~~~~~~
1. Run CMD and navigate to the folder where FindSuspiciousProcesses.exe is located.

2. Run FindSuspiciousProcesses.exe and pass the IP address To check as a parameter. For example - FindSuspiciousProcesses.exe 157.240.20.52.

3. You need to give administrator privileges in order for the program to run properly.

4. An output file will be created in the same folder and will show the system information and the processes information, if any.
The output file's name will be suspiciousProcesses-<Date-Time>. For example suspiciousProcesses-08-01-2020-22-03-08.txt.

5. In case of an error, the output file will contain the error.

~~~~~~~~~~~~~~
Documentation:
~~~~~~~~~~~~~~
1. I split the project into 3 main classes - FindSuspiciousProcesses, SystemData and ProcessData.
FindSuspiciousProcesses runs the main program and stores information about the system data and checks the processes which connect to the given IP Address.
SystemData and ProcessData are classes used to sort the data in a more organized way.

2. SystemData contains the following properties:
DWORDLONG totalVirtualMemory - The system's total virtual memory.
DWORDLONG totalPhysicalMemory - The system's total physical memory. 
DWORDLONG virtualMemoryInUse - The system's virtual memory that is currently in use.
DWORDLONG physicalMemoryInUse - The system's physical memory that is currently in use.
double cpuPercentageUsage - The CPU percentage that is currently in use.

3. ProcessData contains the following properties:
bool isValid - Notes if the process is valid or not when retrieving the process's information.
DWORD processID - The process's ID.
string processFullPath - The process's full path to the executable.
SIZE_T virtualMemoryInUse - The amount of virtual memory the process uses.
SIZE_T physicalMemoryInUse - The amount of physical memory the process uses.

4. The Utilities class is a static class which helps in formatting the bytes to a more human readable format.

5. Program Flow:
	1. The program first gets all the processes the connect to any IPv4 address via TCP protocol and keeps their process ID.
	2. For each process, the program gathers the above information, as mentioned in ProcessData.
	3. The program also gets all the above system information, as mentioned in SystemData.
	4. The program outputs all the collected data to a text file which is named "suspiciousProcesses" and the date and time attached to it.
	
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Total time for completing the task:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Approximately 7 hours.

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Things I would have done/improved if I had more time:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
1. I would gather more network traffic information on the processes - The problem is the C++ API is not that clear and well documented.
2. Same goes for general system and process information.
3. I would write the program as a service that would run every given time interval and write reports to a file or even send to a server.